from django.shortcuts import  render, redirect
from django.utils.translation import ugettext_lazy as _

from django.views.generic import TemplateView, ListView

from .forms import ProductForm
from .models import Product

def home(request):
    context = {'title': 'Welcome to our Catalog!'}
    return render(request, 'home.html', context)


class HomeView(TemplateView):
    template_name  = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['title'] = "Welcome to our Catalog!"
        return context


class ProductListView(ListView):
    model = Product
    template_name  = "product_list.html"

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        context['title'] = "Product list"
        return context


def create_product(request):
    form = ProductForm(request.POST or None)
    if form.is_valid():
        # Creates an instance of Product 
        form.save()
        return redirect('home')
    else:
        context = {'form': form, 'title': 'Create product'}
        return render(request, 'create_product.html', context)


def edit_product(request, product_id):
    product = Product.objects.get(pk=product_id)
    form = ProductForm(request.POST or None, instance=product)
    if form.is_valid():
        form.save()
        return redirect('list')
    else:
        context = {'form': form, 'title': product.name}
        return render(request, 'create_product.html', context)
