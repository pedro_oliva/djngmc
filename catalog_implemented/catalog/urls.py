from django.conf.urls import patterns, include, url

from django.contrib import admin

from .views import ProductListView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'catalog.views.home', name='home'),
    url(r'^list/$', ProductListView.as_view(), name='list'),
    url(r'^create/$', 'catalog.views.create_product', name='create_product'),
    url(r'^edit/(?P<product_id>\d+)/$', 'catalog.views.edit_product', name='edit_product'),
    url(r'^admin/', include(admin.site.urls)),
)
